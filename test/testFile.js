const sumar = require("../index");
const assert = require("assert");

// Afirmación 
describe("Probar la suma de dos numeros", ()=>{
    // Afirmar que cinco mas cinco es diez
    it('Cinco mas cinco es diez', ()=>{
        assert.equal(10, sumar(5,5));
    });
    // Afirmar que cinco mas cinco no son cicuenta y cinco 
    it('Afirmar que cinco mas cinco no son cicuenta y cinco', ()=>{
        assert.notEqual("55", sumar(5,5));
    });
});